var express = require('express');
var request = require('request');
var bodyParser = require('body-parser');

// Create our app
var app = express();
const PORT = process.env.PORT || 3000;

var parseHeaders = function(req) {
    var headers = {
        'content-type': req.header('content-type')
    };
    var authHeader = req.header('x-auth-token');

    if (authHeader && authHeader !== 'null') {
        headers['x-auth-token'] = authHeader;
    }

    return headers;
}

// MIDDLEWARE
app.use(function (req, res, next) {
    if (req.headers['x-forwarded-proto'] === 'https') {
        res.redirect('http://' + req.hostname + req.url);
    } else {
        next();
    }
});

app.use(bodyParser.json());

app.post('/proxy/:url', function (req, res) {
    var options = {
        method: 'POST',
        url: req.params.url,
        headers: parseHeaders(req),
        body: req.body,
        json: true
    };

    request(options, function (error, response, body) {
        if (error) {
            res.status(500).send("Fail!\n", JSON.stringify(error));
        } else {
            res.status(200).send(body);
        }
    });
});

app.get('/proxy/:url', function (req, res) {
    var options = {
        method: 'GET',
        url: req.params.url,
        headers: parseHeaders(req),
        json: true
    };

    request(options, function (error, response, body) {
        if (error) {
            res.status(500).send("Fail!\n", JSON.stringify(error));
        } else {
            res.status(200).send(body);
        }
    });
});

app.delete('/proxy/:url', function (req, res) {
    var options = {
        method: 'DELETE',
        url: req.params.url,
        headers: parseHeaders(req),
        json: true
    };

    request(options, function (error, response, body) {
        if (error) {
            res.status(500).send("Fail!\n", JSON.stringify(error));
        } else {
            res.status(200).send(body);
        }
    });
});

app.put('/proxy/:url', function (req, res) {
    var options = {
        method: 'PUT',
        url: req.params.url,
        headers: parseHeaders(req),
        body: req.body,
        json: true
    };

    request(options, function (error, response, body) {
        if (error) {
            res.status(500).send("Fail!\n", JSON.stringify(error));
        } else {
            res.status(200).send(body);
        }
    });
});

app.use(express.static('public'));

app.listen(PORT, function () {
    console.log('Express server is up on port ' + PORT);
});