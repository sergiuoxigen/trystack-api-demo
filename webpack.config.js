var webpack = require('webpack');

module.exports = {
    entry: ['./app/app.jsx'],
    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery'
        })
    ],
    output: {
        path: __dirname,
        filename: './public/bundle.js'
    },
    resolve: {
        modules: [__dirname, 'node_modules', './app/components', './app/stores', './app/actions', './app/tools'],
        extensions: ['*', '.js', '.jsx']
    },
    module: {
        rules: [{
            loader: 'babel-loader',
            query: {
                presets: ['react', 'es2015', 'stage-0']
            },
            test: /\.jsx?$/,
            exclude: /(node_modules|bower_components)/
        }, {
            test: /\.css$/,
            use: ['style-loader', 'css-loader']
        }]
    },
    devtool: 'cheap-module-eval-source-map'
};