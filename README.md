# TryStack Nova API Demo

A responsive UI build using React for managing TryStack compute servers.
It uses RefluxJS as a dataflow architecture, [Styled Components](https://www.styled-components.com) for application styling with [Foundation](http://foundation.zurb.com/) as the base styling for all HTML elements.
Unit tests are run on [Kharma](https://karma-runner.github.io/) for real device testing with [MochaJS](https://mochajs.org/) as the test reporter and [Expect](https://github.com/mjackson/expect) as the assertion library.

The application is live on [Heroku](https://vast-atoll-47978.herokuapp.com/). Login details are filled in.

*ExpressJS is used as a server for the application. All calls to TryStack API are made using the Express server as a proxy because of TryStack CORS issues.*

## Screenshots

Server details are pulled showing server status; servers are refreshed from time to time if one of them has a task state.

![Screenshot 1](screenshots/screenshot-1.png)

Servers can be started or stopped.

![Screenshot 2](screenshots/screenshot-2.png)

Servers can be renamed.

![Screenshot 3](screenshots/screenshot-3.png)

Servers can be created. TryStack allows a maximum of 3 servers. *Cirros-0.3.4* is used as the server image.

![Screenshot 4](screenshots/screenshot-4.png)

Servers can be deleted. Deleted servers may appear in the list with a *DELETED* status, in which case the list will be refreshed.

![Screenshot 5](screenshots/screenshot-5.png)

The *Login* interface.

![Screenshot 6](screenshots/screenshot-6.png)

Tests are run by rendering different components in the Chrome browser.

![Screenshot 7](screenshots/screenshot-7.png)

