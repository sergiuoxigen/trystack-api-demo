import expect from 'expect';
import React from 'react';
import ReactDOM from 'react-dom';
import TestUtils from 'react-addons-test-utils';
import $ from 'jQuery';

import Servers from 'Servers';

describe('Servers', () => {
    it('should exist', () => {
        expect(Servers).toExist();
    });

    describe('render', () => {
        it('should render header', () => {
            let servers = TestUtils.renderIntoDocument(<Servers />);

            let el = $(ReactDOM.findDOMNode(servers));
            let headerText = el.find('h4').text();

            expect(headerText).toBe('Nova Servers');
        });

        it('should render list', () => {
            let servers = TestUtils.renderIntoDocument(<Servers />);

            servers.setState({
                servers: [
                    {
                        id: 'server-1',
                        status: 'ACTIVE',
                        name: 'Server 1'
                    },
                    {
                        id: 'server-2',
                        status: 'SHUTOFF',
                        name: 'Server 2'
                    }
                ]
            })

            let el = $(ReactDOM.findDOMNode(servers));
            let items = el.find('.server-item');

            expect(items.length).toBe(2);
        });

        it('should open add server modal on add server button click', () => {
            let servers = TestUtils.renderIntoDocument(<Servers />);
            let el = $(ReactDOM.findDOMNode(servers));

            TestUtils.Simulate.click(el.find('.add-button')[0]);

            expect(servers.state.addServerModal).toBeTruthy();
        });
    });
});