import expect from 'expect';
import React from 'react';
import ReactDOM from 'react-dom';
import TestUtils from 'react-addons-test-utils';
import $ from 'jQuery';

import ServerItem from 'ServerItem';

describe('ServerItem', () => {
    it('should exist', () => {
        expect(ServerItem).toExist();
    });

    it('should call loading handler on delete confirmation', () => {
        var loadingSpy = expect.createSpy();
        let serverItem = TestUtils.renderIntoDocument(<ServerItem onLoading={loadingSpy} />);
        let el = $(ReactDOM.findDOMNode(serverItem));

        serverItem.handleDeleteConfirmation();

        expect(loadingSpy).toHaveBeenCalled();
    });
});