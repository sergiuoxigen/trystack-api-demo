import React from 'react';
import Reflux from 'reflux';
import styled from 'styled-components';
import { colors } from '../constants/Style';

import ServerStore from 'ServerStore';
import ServerActions from 'ServerActions';
import UserStore from 'UserStore';

import ServerItem from 'ServerItem';
import ModalDialog from 'ModalDialog';
import Spinner from 'Spinner';

const Header = styled.div`
    display: flex;
    align-items: center;
    border-bottom: 1px solid ${colors.gray};
    margin-bottom: 15px;
`;

const Title = styled.h4`
    margin-bottom: 15px;
    flex-grow: 1;
`;

const AddButton = styled.button`
    font-weight: bold;
`;

class Servers extends Reflux.Component {
    constructor(props) {
        super(props);

        this.store = ServerStore;
        this.state = { servers: null, loading: true };

        ServerActions.loadServers();
    }

    addServerHandler() {
        this.setState({ addServerModal: true });
    }

    newServerCancelHandler() {
        this.setState({ addServerModal: false });
    }

    newServerAddHandler(serverName) {
        this.setState({ addServerModal: false, loading: true });
        ServerActions.addServer(serverName);
    }

    serverItemLoadingHandler(loading) {
        this.setState({ loading: loading });
    }

    render() {
        let addServerModal = () => {
            if (!this.state.addServerModal) {
                return null;
            }

            return <ModalDialog title="Add new server" inputPlaceholder="Server Name" onSave={this.newServerAddHandler.bind(this)} onCancel={this.newServerCancelHandler.bind(this)} />;
        };

        return (
            <div>
                <Header>
                    <Title>Nova Servers</Title>
                    {this.state.loading ? <Spinner /> : null}
                    <AddButton type="button" className="button hollow add-button" onClick={this.addServerHandler.bind(this)}>+</AddButton>
                </Header>

                {this.state.servers.map(s => {
                    return <ServerItem onLoading={this.serverItemLoadingHandler.bind(this)} key={s.id} {...s} />
                })}

                {addServerModal()}
            </div>
        );
    }
}

export default Servers;