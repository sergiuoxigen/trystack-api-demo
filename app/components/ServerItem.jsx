import React from 'react';
import Reflux from 'reflux';
import styled from 'styled-components';

import ServerStore from 'ServerStore';
import ServerActions from 'ServerActions';
import UserStore from 'UserStore';

import ModalDialog from 'ModalDialog';

const Root = styled.div`
    display: flex;
    align-items: center;

    @media (max-width: 600px) {
        margin-bottom: 35px;
    }
`;

const Name = styled.div`
    margin-bottom: 15px;
    flex-grow: 1;
`;

const Buttons = styled.div`
    @media (max-width: 600px) {
        width: 100px;

        & button {
            width: 95px;
        }
    }
`;

const Button = styled.button`
    margin-left: 5px !important;
    font-size: 12px !important;
`;

const Status = styled.span`
    font-size: 12px !important;
    vertical-align: middle;
    margin-right: 5px;
    padding: 5px !important;
    text-transform: lowercase;
    width: 50px;
    text-align: center;
    background: #ececec !important;
    color: #444444 !important;
`;

class ServerItem extends Reflux.Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    handleDeleteClick() {
        this.setState({ showDeleteConfirmation: true });
    }

    handleDeleteCancel() {
        this.setState({ showDeleteConfirmation: false });
    }

    handleDeleteConfirmation() {
        this.setState({ showDeleteConfirmation: false });
        this.props.onLoading(true);
        ServerActions.deleteServer(this.props.id);
    }

    handleRenameClick() {
        this.setState({ showRenameDialog: true });
    }

    handleRenameCancel() {
        this.setState({ showRenameDialog: false });
    }

    handleRenameConfirmation(newName) {
        this.setState({ showRenameDialog: false });
        this.props.onLoading(true);
        ServerActions.renameServer(this.props.id, newName);
    }

    handleServerToggleClick() {
        this.props.onLoading(true);
        ServerActions.toggleServer(this.props.id, this.props.status);
    }

    render() {
        let renderDeleteConfirmation = () => {
            if (!this.state.showDeleteConfirmation) {
                return null;
            }

            return <ModalDialog title="Delete Server" message={`Are you sure you want to delete '${this.props.name}' server?`} confirmationDialog={true} onCancel={this.handleDeleteCancel.bind(this)} onSave={this.handleDeleteConfirmation.bind(this)} />;
        }

        let renderRenameDialog = () => {
            if (!this.state.showRenameDialog) {
                return null;
            }

            return <ModalDialog title="Rename Server" inputValue={this.props.name} inputPlaceholder="New name" onCancel={this.handleRenameCancel.bind(this)} onSave={this.handleRenameConfirmation.bind(this)} />;
        }

        return (
            <Root className="server-item">
                <Name>
                    <Status className="label secondary">{this.props.status}</Status>{this.props.name}
                </Name>
                <Buttons>
                    {this.props.status === 'ACTIVE' || this.props.status === 'SHUTOFF' ?
                        <Button type="button" className="warning button hollow" onClick={this.handleServerToggleClick.bind(this)}>{this.props.status === 'ACTIVE' ? 'Stop' : 'Start'}</Button> : null
                    }
                    <Button type="button" className="secondary button hollow" onClick={this.handleRenameClick.bind(this)}>Rename</Button>
                    <Button type="button" className="alert button hollow" onClick={this.handleDeleteClick.bind(this)}>Delete</Button>
                </Buttons>

                {renderDeleteConfirmation()}
                {renderRenameDialog()}
            </Root>
        );
    }
}

export default ServerItem;