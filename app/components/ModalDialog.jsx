import React from 'react';
import styled from 'styled-components';
import { colors } from '../constants/Style';

const Root = styled.div`
    position: absolute;
    background: rgba(0, 0, 0, 0.3);
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
`;

const Dialog = styled.div`
    border: 1px solid ${colors.gray};
    background: white;
    max-width: 300px;
    padding: 20px;
    margin: auto;
    margin-top: 5%;
    border-radius: 3px;
`;

const Button = styled.button`
    margin-right: 5px !important;
`;

const Message = styled.div`
    margin-bottom: 10px;
`;

class Modal extends React.Component {
    constructor(props) {
        super(props);

        this.state = { inputValue: props.inputValue || '' };
    }

    componentDidMount() {
        this.refs.textInput && this.refs.textInput.focus();
    }

    inputChangeHandler(e) {
        this.setState({ inputValue: e.target.value });
    }

    inputKeyPressHandler(e) {
        if (e.key === "Enter") {
            this.saveClickHandler();
        }
    }

    saveClickHandler() {
        if (this.props.onSave) {
            this.props.onSave(this.state.inputValue);
        }
    }

    cancelClickHandler() {
        if (this.props.onCancel) {
            this.props.onCancel(this.state.inputValue);
        }
    }

    render() {
        return (
            <Root>
                <Dialog>
                    <h4>{this.props.title}</h4>
                    <div>
                        <Message>{this.props.message}</Message>
                        {
                            !this.props.confirmationDialog ?
                                <input
                                    ref="textInput"
                                    type="text"
                                    placeholder={this.props.inputPlaceholder}
                                    value={this.state.inputValue}
                                    onChange={this.inputChangeHandler.bind(this)}
                                    onKeyPress={this.inputKeyPressHandler.bind(this)} /> : null
                        }
                        <div>
                            <Button
                                type="button"
                                className={this.props.confirmationDialog ? 'button alert hollow' : 'button success hollow'}
                                onClick={this.saveClickHandler.bind(this)}>{this.props.confirmationDialog ? 'Yes' : 'Save'}</Button>
                            <Button
                                type="button"
                                className="button secondary hollow"
                                onClick={this.cancelClickHandler.bind(this)}>{this.props.confirmationDialog ? 'No' : 'Cancel'}</Button>
                        </div>
                    </div>
                </Dialog>
            </Root>
        );
    }
}

export default Modal;