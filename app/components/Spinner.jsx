import React from 'react';
import styled from 'styled-components';
import { colors } from '../constants/Style';

const Border = `2px solid ${colors.light}`;
const Loader = styled.div`
    border-radius: 50%;
    width: 20px;
    height: 20px;
    margin-bottom: 15px;
    margin-right: 10px;
    border-top: ${Border};
    border-right: ${Border};
    border-bottom: ${Border};
    border-left: 2px solid ${colors.dark};
    transform: translateZ(0);
    animation: rotation 1.1s infinite linear;

    @keyframes rotation {
        0% {
            transform: rotate(0deg);
        }
        100% {
            transform: rotate(360deg);
        }
    }
`;

class Spinner extends React.Component {

    render() {
        return (
            <Loader />
        );
    }
}

export default Spinner;