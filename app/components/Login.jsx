import React from 'react';
import PropTypes from 'prop-types';
import Reflux from 'reflux';
import styled from 'styled-components';

import UserActions from 'UserActions';
import UserStore from 'UserStore';
import Spinner from 'Spinner';

const Root = styled.div`
    width: 70%;
    max-width: 500px;
    margin: auto;
`;

const Title = styled.h4`
    text-align: center;
`;

const Buttons = styled.div`
    display: flex;
    align-items: center;
`;

const Button = styled.button`
    margin-right: 10px !important;
`;

class Login extends Reflux.Component {

    constructor(props) {
        super(props);
        this.store = UserStore;
        this.state = {};
    }

    handleSubmit(e) {
        e.preventDefault();

        this.setState({ loading: true });
        UserActions.login({
            username: this.state.username,
            password: this.state.password
        });
    }

    handleUsernameChange(e) {
        this.setState({ username: e.target.value });
    }

    handlePasswordChange(e) {
        this.setState({ password: e.target.value });
    }

    render() {
        return (
            <Root>
                <Title>Login</Title>
                <form onSubmit={this.handleSubmit.bind(this)} className="form">
                    <div>
                        <label>Username
                            <input type="text" value={this.state.username} onChange={this.handleUsernameChange.bind(this)} placeholder="Type username" />
                        </label>
                    </div>
                    <div>
                        <label>Password
                            <input type="password" value={this.state.password} onChange={this.handlePasswordChange.bind(this)} placeholder="Type password" />
                        </label>
                    </div>
                    <Buttons>
                        <Button type="submit" className="button primary hollow">Submit</Button>
                        {this.state.loading ? <Spinner /> : null}
                    </Buttons>
                </form>
            </Root>
        );
    }
}

export default Login;