import React from 'React';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { colors } from '../constants/Style';

const Header = styled.h3`
    background: ${colors.light};
    padding: 15px 50px;
    text-align: center;
    color: white;
`;

const Content = styled.div`
    width: 70%;
    max-width: 500px;
    margin: auto;
`;

const Root = styled.div`
    min-width: 300px;
`;

class Layout extends React.Component {
    static propTypes = {
        children: PropTypes.element.isRequired
    }

    render() {
        return (
            <Root>
                <Header>TryStack Demo</Header>
                <Content>
                    {this.props.children}
                </Content>
            </Root>
        )
    }
}

export default Layout;