import Reflux from 'reflux';
import ApiRequest from 'ApiRequest';
import { tokensUrl, proxyUrl } from '../config';

let UserActions = Reflux.createActions({
    login: { children: ["success", "failed"] }
});

UserActions.login.listen(user => {
    let body = {
        auth: {
            tenantName: user.username,
            passwordCredentials: {
                username: user.username,
                password: user.password
            }
        }
    }

    ApiRequest.send({
        url: proxyUrl + encodeURIComponent(tokensUrl),
        method: "POST",
        headers: { "X-Auth-Token": null },
        body
    }).then(data => {
        UserActions.login.success(data);
    }, UserActions.login.failed)
});

export default UserActions;