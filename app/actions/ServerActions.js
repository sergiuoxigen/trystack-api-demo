import Reflux from 'reflux';
import ApiRequest from 'ApiRequest';
import { proxyUrl, imageRef } from '../config';
import Cookies from 'js-cookie';

let ServerActions = Reflux.createActions({
    loadServers: { children: ["success", "failed"] },
    addServer: { children: ["success", "failed"] },
    deleteServer: { children: ["success", "failed"] },
    renameServer: { children: ["success", "failed"] },
    toggleServer: { children: ["success", "failed"] }
});

let getCurrentUser = () => {
    var currentUser = {
        token: Cookies.get('user-token'),
        computeEndpoint: proxyUrl + Cookies.get('compute-end-point') + encodeURIComponent('/servers')
    }

    if (!currentUser.token) {
        window.location.replace('/#/login');
        return null;
    }

    return currentUser;
}

ServerActions.loadServers.listen(() => {
    var currentUser = getCurrentUser();

    if (!currentUser) {
        return;
    }

    ApiRequest.send({
        url: currentUser.computeEndpoint + encodeURIComponent("/detail"),
        method: "GET",
        headers: { "X-Auth-Token": currentUser.token },
    }).then(data => {
        ServerActions.loadServers.success(data);
    }, ServerActions.loadServers.failed)
});

ServerActions.addServer.listen(serverName => {
    var currentUser = getCurrentUser();

    if (!currentUser) {
        return;
    }

    let body = {
        server: {
            name: serverName,
            imageRef,
            flavorRef: "1"
        }
    };

    ApiRequest.send({
        url: currentUser.computeEndpoint,
        method: "POST",
        headers: { "X-Auth-Token": currentUser.token },
        body
    }).then(data => {
        ServerActions.addServer.success(data);
    }, ServerActions.addServer.failed)
});

ServerActions.deleteServer.listen(serverId => {
    var currentUser = getCurrentUser();

    if (!currentUser) {
        return;
    }

    ApiRequest.send({
        url: currentUser.computeEndpoint + encodeURIComponent('/' + serverId),
        method: "DELETE",
        headers: { "X-Auth-Token": currentUser.token }
    }).then(ServerActions.deleteServer.success, ServerActions.deleteServer.failed)
});

ServerActions.renameServer.listen((serverId, newServerName) => {
    var currentUser = getCurrentUser();

    if (!currentUser) {
        return;
    }

    let body = { server: { name: newServerName } };

    ApiRequest.send({
        url: currentUser.computeEndpoint + encodeURIComponent('/' + serverId),
        method: "PUT",
        headers: { "X-Auth-Token": currentUser.token },
        body
    }).then(ServerActions.renameServer.success, ServerActions.renameServer.failed)
});

ServerActions.toggleServer.listen((serverId, currentStatus) => {
    let currentUser = getCurrentUser();

    if (!currentUser) {
        return;
    }

    var body = { "os-start": null };

    if (currentStatus === "ACTIVE") {
        body = { "os-stop": null };
    }

    ApiRequest.send({
        url: currentUser.computeEndpoint + encodeURIComponent(`/${serverId}/action`),
        method: "POST",
        headers: { "X-Auth-Token": currentUser.token },
        body
    }).then(ServerActions.toggleServer.success, ServerActions.toggleServer.failed)
});

export default ServerActions;