import React from 'react';
import ReactDOM from 'react-dom';
import { Switch, Route, HashRouter } from 'react-router-dom'

import Layout from 'Layout';
import Servers from 'Servers';
import Login from 'Login';

require('foundation-sites/dist/js/foundation.min.js');
require('foundation-sites/dist/css/foundation.min.css');
$(document).foundation();

ReactDOM.render(
    <HashRouter>
        <Layout>
            <Switch>
                <Route exact path='/' component={Servers} />
                <Route path='/login' component={Login} />
            </Switch>
        </Layout>
    </HashRouter>,
    document.getElementById('app')
);