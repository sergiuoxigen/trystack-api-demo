import Reflux from 'reflux';
import ServerActions from 'ServerActions';

class ServerStore extends Reflux.Store {

    constructor() {
        super()

        this.listenables = ServerActions;
        this.state = { servers: [] };
    }

    onLoadServersSuccess(data) {
        this.setState({
            servers: data.servers,
            loading: false
        });

        let pending = this.state.servers.find(s => s.status === 'BUILD' || s.status === 'DELETED' || s['OS-EXT-STS:task_state'] !== null);

        if (pending) {
            this.setState({ loading: true });
            setTimeout(() => { ServerActions.loadServers(); }, 1000);
        }
    }

    onAddServerSuccess(data) {
        ServerActions.loadServers();

        // quota exceeded
        if (data.forbidden) {
            alert(data.forbidden.message);
        }
    }

    onAddServerFailed(req) {
        alert(`Server adding failed!\n${req.status}`);
    }

    onDeleteServerSuccess() {
        ServerActions.loadServers();
    }

    onDeleteServerFailed(req) {
        alert(`Server deletion failed!\n${req.status}`);
    }

    onLoadServersFailed(req) {
        alert(`Servers loading failed!\n${req.status}`);
    }

    onRenameServerSuccess() {
        ServerActions.loadServers();
    }

    onRenameServerFailed() {
        alert(`Server renaming failed!\n${req.status}`);
    }

    onToggleServerSuccess() {
        ServerActions.loadServers();
    }

    onToggleServerFailed() {
        alert(`Server toggle failed!\n${req.status}`);
    }
}

export default ServerStore;