import Reflux from 'reflux';
import UserActions from 'UserActions';
import Cookies from 'js-cookie';

class UserStore extends Reflux.Store {

    constructor() {
        super()

        this.listenables = UserActions
        
        this.state = {
            username: "facebook10209879667016819",
            password: "AFYReo10Ae06mBD1",
            currentUser: {
                token: Cookies.get('user-token'),
                computeEndpoint: decodeURIComponent(Cookies.get('compute-end-point'))
            }
        }
    }

    onLoginSuccess(data) {
        this.setState({
            currentUser: {
                token: data.access.token.id,
                computeEndpoint: data.access.serviceCatalog.find(c => c.type === "compute").endpoints[0].publicURL
            }
        })

        Cookies.set('user-token', this.state.currentUser.token, { expires: 1 / 24 });
        Cookies.set('compute-end-point', encodeURIComponent(this.state.currentUser.computeEndpoint), { expires: 1 / 24 });

        window.location.replace('/#/');
    }

    onLoginFailed(req, status, err) {
        alert(`Login failed!\n${req.status}`);
    }
}

UserStore.id = "userStore";
export default UserStore;