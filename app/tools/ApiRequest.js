let instance = null;

class ApiRequest {
    constructor() {
        if (!instance) {
            instance = this;
        }

        return instance;
    }

    headers = {
        "content-type": "application/json"
    }

    send(options) {
        this.headers = {
            ...this.headers,
            ...options.headers
        }

        return new Promise((resolve, reject) => {
            let settings = {
                url: options.url,
                method: options.method || "GET",
                headers: this.headers,
                crossDomain: true,
                async: true,
                processData: false,
                data: JSON.stringify(options.body)
            }

            $.ajax(settings).done(data => {
                resolve(data);
            }).fail((req, status, error) => {
                reject({ req, status, error });
            });
        });
    }
}

export default new ApiRequest;